package gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

/**
 * Criado por vitor em 20/09/18
 **/

@Singleton
public class GsonSingleton {

    @Produces
    public Gson getGson() {
        return new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
    }


}
