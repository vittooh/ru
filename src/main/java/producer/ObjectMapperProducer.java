package producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr353.JSR353Module;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

@ApplicationScoped
public class ObjectMapperProducer {

    private static final ObjectMapper objectMapper = new ObjectMapper() {{
        registerModule(new JSR353Module());
    }};

    @Produces
    @Singleton
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
