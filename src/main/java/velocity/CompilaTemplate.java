package velocity;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.StringWriter;
import java.util.HashMap;

@LocalBean
@Stateless
public class CompilaTemplate {


    public String retornaTemplateMarmitex(HashMap<String, Object> parametros) {
        Velocity.init();
        VelocityContext velocityContext = new VelocityContext();
        parametros.forEach(velocityContext::put);

        StringWriter stringWriter = new StringWriter();
        String template = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Marmitex Retirada</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style=\"background: aquamarine;width: 300px;height: 60px;vertical-align:center\" >\n" +
                "    <h3> Atenção ${UsuarioPojo}</h3>\n" +
                "    <h5>Seu marmitex ficara pronto as ${hora}!!</h5>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
        Velocity.evaluate(velocityContext, stringWriter, "mail", template);

        return stringWriter.toString();
    }
}
