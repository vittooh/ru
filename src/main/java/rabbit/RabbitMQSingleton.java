package rabbit;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.ejb.EJBException;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Singleton
public class RabbitMQSingleton {


    @Inject
    ConnectionFactory connectionFactory;

    @Produces
    @ApplicationScoped
    public ConnectionFactory getFactory(){
        // nao era necessario mas se um dia tiver que setar o host ta ai
        ConnectionFactory factory = new ConnectionFactory();
        factory.setAutomaticRecoveryEnabled(true);
        factory.setRequestedHeartbeat(25);
        return factory;
    }


    @Produces
    @ApplicationScoped
    public Connection getConnection(){
        try {
        return  connectionFactory.newConnection();
        } catch (IOException | TimeoutException e) {
            throw  new EJBException(e);
        }

    }
}
