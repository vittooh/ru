package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(AppRest.REST_PATH)
public class AppRest extends Application {

    public static final String REST_PATH = "resources";
}
