package rest;

import dao.*;
import pojo.sessao.SessaoPojo;
import pojo.sessao.StatusSessaoPojo;
import utils.CalendarUtils;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Produces({APPLICATION_JSON})
@Consumes({APPLICATION_JSON})
@LocalBean
@Stateless
public class SessaoResource {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    private static final SimpleDateFormat sdfDay = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Inject
    @DaoSessao
    private SessaoDAO dao;

    @Inject
    UsuarioDao usuarioDao;

    @Inject
    RefeicaoDao refeicaoDao;

    @POST
    @Path("/cria")
    public Response criaSessao(SessaoPojo sessao) {
        if (dao.retornaSessaoPorUsuario(sessao.getIdUser()) != null) {
            return Response.status(500).entity("Usuário já possui uma reserva ativa!!!!").build();
        }

        if (!usuarioDao.usuarioTemTicket(sessao.getIdUser(), false, 1L)) {
            return Response.status(500).entity("Usuário não possui creditos").build();
        }

        if (refeicaoDao.excedeuLimiteRefeicoes(sessao.getIdUser(), 1L))
            return Response.status(500).entity("Usuário excedeu a quantidade de  refeições no R.U  por dia!!")
                    .build();


        dao.criaSessao(sessao.getSessao(), sessao.getIdUser(), sessao.getCadeiraID());
        return Response.ok().build();
    }


    @GET
    @Path("/cadeiras")
    public Response retornaCadeirasLivres() {
        return Response.ok(dao.retornaCadeiras()).build();
    }

    @GET
    @Path("/sessaoUser")
    public Response retornaSessoesUsuario(@QueryParam("idUser") Long idUser, @QueryParam("start") Long start, @QueryParam("end") Long end) {
        return Response.ok(dao.retornaSessaoPojo(idUser, start, end)).build();
    }

}
