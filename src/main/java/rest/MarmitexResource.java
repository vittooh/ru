package rest;

import dao.MarmitexDao;
import dao.RefeicaoDao;
import dao.UsuarioDao;
import entidade.Marmitex;
import entidade.Usuario;
import entidade.UsuarioTipo;
import pojo.marmitex.MarmitexPojo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Calendar;

@LocalBean
@Stateless
public class MarmitexResource {

    @Inject
    UsuarioDao usuarioDao;

    @Inject
    MarmitexDao marmitexDao;

    @Inject
    RefeicaoDao refeicaoDao;


    @POST
    @Path("/cria")
    public Response criaMarmitex(MarmitexPojo marmitexPojo) {
        Usuario u = usuarioDao.retornaUsuarioById(marmitexPojo.getUserID());

        if (u == null) {
            return Response.status(500).entity("Usuário não encontrado!!").build();
        }

        if (u.getUsuarioTipo().equals(UsuarioTipo.NAO_ISENTO) && !usuarioDao.usuarioTemTicket(marmitexPojo.getUserID(), true, marmitexPojo.getQuantidade()))
            return Response.status(500).entity("Usuário não creditos !!").build();

        if (!marmitexDao.possuiMarmitexSemRetirar(marmitexPojo.getUserID()))
            return Response.status(500).entity("Usuário tem marmitex pendentes de retirada!!").build();


        if (refeicaoDao.excedeuLimiteRefeicoes(marmitexPojo.getUserID(), marmitexPojo.getQuantidade()))
            return Response.status(500).entity("Usuário excedeu a quantidade de  refeições no R.U  por dia!!")
                    .build();

        marmitexDao.cria(marmitexPojo);

        return Response.ok().build();
    }

    @POST
    @Path("/baixaMarmitex")
    public Response baixaMarmitex(MarmitexPojo marmitexPojo) {
        Marmitex paraBaixar = marmitexDao.findById(marmitexPojo.getMarmitexID());

        if (paraBaixar == null)
            return Response.status(404)
                    .entity("Marmitex com id " + marmitexPojo.getMarmitexID() + " não encontrada!!")
                    .build();

        if (paraBaixar.getDataCancelamento() != null)
            return Response.ok().entity("Marmitex já foi baixado!!").build();

        paraBaixar.setNotificou(true);
        paraBaixar.setRetirou(false);
        paraBaixar.setCancelada(true);
        paraBaixar.setDataCancelamento(Calendar.getInstance());
        marmitexDao.salva(paraBaixar);

        return Response.ok().entity("Baixa Realizada com sucesso").build();
    }
}
