package rest;

import dao.UsuarioDao;
import entidade.Usuario;
import pojo.usuario.UsuarioPojo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Criado por vitor em 01/11/18
 **/
@LocalBean
@Stateless
public class UsuarioResource {

    @Inject
    UsuarioDao usuarioDao;


    @GET
    @Path("/info/{id}")
    @Produces("application/json")
    public Response getDadosUsuario(@PathParam("id") Long id) {
        Usuario u = usuarioDao.getManager().find(Usuario.class, id);

        if (u == null)
            return Response.serverError().entity("Usuário não encontrado!! Como chegou nesse ponto?!").build();

        return Response.ok(new UsuarioPojo(u.getNome(), u.getUsuarioTipo().toString().equals("ISENTO"), u.getQtCreditos()))
                .build();
    }

}
