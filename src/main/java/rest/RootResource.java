package rest;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.*;

@Path("/")
@Produces({TEXT_XML, APPLICATION_XML, APPLICATION_JSON})
@Consumes({TEXT_XML, APPLICATION_XML, APPLICATION_JSON})
@LocalBean
@Stateless
public class RootResource {

    @Inject
    SessaoResource sessaoResource;

    @Inject
    MarmitexResource marmitexResource;

    @Inject
    UsuarioResource usuarioResource;

    @GET
    @Path("/ola")
    @Produces(value = "text/plain")
    public String retornaOla() {
        return "oi";
    }


    @Path("/sessao")
    public SessaoResource retornaSessao() {
        return sessaoResource;
    }

    @Path("/marmitex")
    public MarmitexResource retornaMarmitexResource() {
        return marmitexResource;
    }

    @Path("/usuario")
    public UsuarioResource retornaUsuarioResource(){
        return usuarioResource;
    }
}
