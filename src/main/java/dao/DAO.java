package dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;
import java.util.HashMap;

@LocalBean
@Stateless
public abstract class DAO {

    @PersistenceContext(unitName = "ru")
    EntityManager manager;

    public <T> void cria(T toPersist) {
        try {
            manager.persist(toPersist);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public <T> T salva(T toSave) {
        try {
            return manager.merge(toSave);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }

    public <T> void remove(T t) {
        try {
            manager.remove(t);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public <T> Collection<T> lista(Class<T> tClass, String query, HashMap<String, Object> parametros) {
        Query q = manager.createNamedQuery(query);

        parametros.forEach(q::setParameter);

        return q.getResultList();
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

}
