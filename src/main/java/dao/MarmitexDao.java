package dao;

import entidade.Marmitex;
import entidade.Usuario;
import pojo.marmitex.MarmitexPojo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;

@LocalBean
@Stateless
public class MarmitexDao extends DAO {

    @Inject
    UsuarioDao usuarioDao;


    public Marmitex findById(Long id) {
        return manager.find(Marmitex.class, id);
    }

    public void cria(MarmitexPojo marmitexPojo) {
        Marmitex marmitex = new Marmitex();
        Calendar dtRetirada = Calendar.getInstance();
        dtRetirada.setTime(marmitexPojo.getHorarioRetirada());
        marmitex.setDataRetirada(dtRetirada);
        marmitex.setUsuario(usuarioDao.retornaUsuarioById(marmitexPojo.getUserID()));
        marmitex.setQt(marmitexPojo.getQuantidade().intValue());
        marmitex.getUsuario().setQtCreditos(marmitex.getUsuario().getQtCreditos() - (marmitex.getQt() * 4));
        marmitex.setNotificou(false);
        cria(marmitex);
        usuarioDao.salva(marmitex.getUsuario());
    }

    public Collection<Marmitex> retornaMarmitexNotificacao(HashMap<String, Object> param) {
        return lista(Marmitex.class, "marmitex_retirada", param);
    }

    public boolean possuiMarmitexSemRetirar(Long idUser) {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("id", idUser);
        return lista(Marmitex.class, "marmitex_byUserSemRetirar", parametros).isEmpty();
    }

    public int retornaQuantidadeMarmitexUsuarioHoje(Long id) {
        Calendar inicio = Calendar.getInstance();
        inicio.set(Calendar.HOUR_OF_DAY, 0);
        inicio.set(Calendar.MINUTE, 0);
        inicio.set(Calendar.SECOND, 0);
        Calendar finalData = Calendar.getInstance();
        finalData.set(Calendar.HOUR_OF_DAY, 23);
        finalData.set(Calendar.MINUTE, 59);
        finalData.set(Calendar.SECOND, 59);
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("id", id);
        parametros.put("dataInicial", inicio);
        parametros.put("dataFinal", finalData);
        return lista(Marmitex.class, "marmitex_byUserDataRetirada", parametros)
                .stream().map(Marmitex::getQt).reduce((x, y) -> x + y).orElse(0);
    }
}
