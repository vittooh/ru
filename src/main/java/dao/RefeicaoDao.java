package dao;

import javax.inject.Inject;

/**
 * Criado por vitor em 05/11/18
 **/

public class RefeicaoDao extends DAO {

    @Inject
    SessaoDAO sessaoDAO;

    @Inject
    MarmitexDao marmitexDao;


    public boolean excedeuLimiteRefeicoes(Long idUSer, Long qtItem) {
        int qt = marmitexDao.retornaQuantidadeMarmitexUsuarioHoje(idUSer);

        qt += sessaoDAO.retornaSessaoUsuarioHoje(idUSer);

        qt += qtItem;

        return qt > 2;
    }
}
