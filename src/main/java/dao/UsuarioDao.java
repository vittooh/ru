package dao;

import entidade.Usuario;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.HashMap;

@LocalBean
@Stateless
public class UsuarioDao extends DAO {


    public Usuario retornaUsuarioById(Long id) {
        return manager.find(Usuario.class, id);
    }

    public boolean usuarioTemTicket(Long id, boolean marmitex, Long qt) {
        Usuario u = retornaUsuarioById(id);
        return u.getQtCreditos() != 0 && (!marmitex ? u.getQtCreditos() - 3 > 0.0 : u.getQtCreditos() - (4 * qt) > 0.0);
    }

}
