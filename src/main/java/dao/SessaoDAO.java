package dao;

import entidade.Cadeira;
import entidade.Sessao;
import pojo.sessao.CadeiraPojo;
import pojo.sessao.SessaoPojo;
import pojo.sessao.SessaoUsuarioEvent;
import pojo.sessao.StatusSessaoPojo;
import utils.CalendarUtils;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@DaoSessao
@LocalBean
@Stateless
public class SessaoDAO extends DAO {

    @Inject
    private
    UsuarioDao usuarioDao;


    public Sessao retornaSessaoPorUsuario(Long idUser) {
        Collection<Sessao> sessoes = retornaSessoesPorUsuario(idUser, 0L, 100L);

        return sessoes.iterator().hasNext() ? sessoes.iterator().next() : null;
    }

    public Collection<Sessao> retornaSessoesPorUsuario(Long idUser, Long start, Long end) {
        Collection<Sessao> sessaos = manager.createNamedQuery("sessaoALL.byUser", Sessao.class).setParameter("id", idUser).setFirstResult(start.intValue()).setMaxResults(end.intValue()).getResultList();

        return sessaos.isEmpty() ? new ArrayList<>() : sessaos;
    }


    public List<SessaoPojo> retornaSessaoPojo(Long idUser,Long start,Long end) {
        Collection<Sessao> sessoes = retornaSessoesPorUsuario(idUser,start,end);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdfDay = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        return sessoes.stream()
                .map(it -> {
                    StatusSessaoPojo statusSessaoPojo = null;
                    if (it.getInicioSessao() != null && it.getFimSessao() != null) {
                        if (CalendarUtils.retornaDiferençaMinutos(
                                it.getInicioSessao().getTimeInMillis(),
                                it.getFimSessao().getTimeInMillis()) > 25) {
                            statusSessaoPojo = StatusSessaoPojo.ATRASADA;
                        } else {
                            statusSessaoPojo = StatusSessaoPojo.OK;
                        }
                    } else if (it.getInicioSessao() != null && it.getFimSessao() == null) {
                        statusSessaoPojo = StatusSessaoPojo.EM_PROGRESSO;
                    } else
                        statusSessaoPojo = StatusSessaoPojo.NAO_INICIADA;

                    return new SessaoPojo(sdf.format(it.getSessao().getTime()),
                            it.getInicioSessao() != null ? sdfDay.format(it.getInicioSessao().getTime()) : "",
                            it.getFimSessao() != null ? sdfDay.format(it.getFimSessao().getTime()) : "",
                            statusSessaoPojo,
                            it.getId());
                })
                .collect(Collectors.toList());

    }

    public Sessao retornaSessaoPorUsuario(String matricula) {
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("matricula", matricula);
        Collection<Sessao> sessaos = lista(entidade.Sessao.class, "sessaoAtiva.byMatricula", parametros);

        return sessaos.isEmpty() ? null : sessaos.iterator().next();
    }

    public void criaSessao(String sessao, Long idUsuario, Long cadeiraID) {
        Cadeira c = manager.find(Cadeira.class, cadeiraID);
        Sessao s = new Sessao();
        s.setUsuario(usuarioDao.retornaUsuarioById(idUsuario));
        s.getUsuario().setQtCreditos(s.getUsuario().getQtCreditos() - 3);
        Calendar sessaoHora = Calendar.getInstance();
        try {
            sessaoHora.setTime(new SimpleDateFormat("HH:mm:ss").parse(sessao));
            sessaoHora.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            sessaoHora.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            sessaoHora.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        s.setCadeira(c);
        s.setSessao(sessaoHora);
        s.setPrimeiroEvento(true);
        cria(s);
        usuarioDao.salva(s.getUsuario());
    }

    public Collection<Long> cadeirasOcupadas() {
        return lista(Long.class, "sessao.CadeirasOcupadas", new HashMap<>());
    }

    public Collection<Long> cadeirasVazias() {
        Collection<Long> cadeirasOcupadas = cadeirasOcupadas();
        if (cadeirasOcupadas.isEmpty()) {
            return lista(Long.class, "cadeirasAll", new HashMap<>());
        }
        HashMap<String, Object> param = new HashMap<>();
        param.put("lista", cadeirasOcupadas);
        return lista(Long.class, "cadeirasVazias", param);
    }

    public Collection<CadeiraPojo> retornaCadeiras() {
        Collection<CadeiraPojo> cadeiraPojos = new ArrayList<>();
        cadeirasOcupadas().forEach(it -> cadeiraPojos.add(new CadeiraPojo(it, true)));
        cadeirasVazias().forEach(it -> cadeiraPojos.add(new CadeiraPojo(it, false)));
        return cadeiraPojos;
    }

    @Asynchronous
    public void trataSessao(SessaoUsuarioEvent sessao) {
        Sessao s = retornaSessaoPorUsuario(sessao.getMatricula());
        if (s.isPrimeiroEvento()) {
            s.setPrimeiroEvento(false);
            salva(s);
        }

        if (s.getInicioSessao() == null) {
            s.setInicioSessao(sessao.getData());
            salva(s);
        }

        s.setFimSessao(sessao.getData());
        salva(s);
    }


    public int retornaSessaoUsuarioHoje(Long id) {
        HashMap<String, Object> parametros = new HashMap<>();
        Calendar inicio = Calendar.getInstance();
        inicio.set(Calendar.HOUR_OF_DAY, 0);
        inicio.set(Calendar.MINUTE, 0);
        inicio.set(Calendar.SECOND, 0);
        Calendar finalData = Calendar.getInstance();
        finalData.set(Calendar.HOUR_OF_DAY, 23);
        finalData.set(Calendar.MINUTE, 59);
        finalData.set(Calendar.SECOND, 59);
        parametros.put("id", id);
        parametros.put("dataInicial", inicio);
        parametros.put("dataFinal", finalData);
        return lista(Sessao.class, "sessaoAtiva.byUserAndData", parametros)
                .size();

    }
}
