package entidade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity

@NamedQueries(value = {
        @NamedQuery(name = "user.bymatricula", query = "select U from Usuario U where U.matricula = :matricula")
})
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private String matricula;

    private Long qtCreditos;

    private Long strikes;

    @NotNull
    private String email;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private UsuarioTipo usuarioTipo;

    @Version
    private Long version;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Long getQtCreditos() {
        return qtCreditos;
    }

    public void setQtCreditos(Long qtCreditos) {
        this.qtCreditos = qtCreditos;
    }

    public Long getStrikes() {
        return strikes;
    }

    public void setStrikes(Long strikes) {
        this.strikes = strikes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public UsuarioTipo getUsuarioTipo() {
        return usuarioTipo;
    }

    public void setUsuarioTipo(UsuarioTipo usuarioTipo) {
        this.usuarioTipo = usuarioTipo;
    }

    public Usuario() {
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
