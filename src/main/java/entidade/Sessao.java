package entidade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

@Entity
@NamedQueries(value = {
        @NamedQuery(name = "sessaoAtiva.byUser", query = "select S from Sessao  S where S.usuario.id  = :id and S.fimSessao is null"),
        @NamedQuery(name = "sessaoAtiva.byUserAndData", query = "select S from Sessao  S where S.usuario.id  = :id  and S.sessao >= :dataInicial " +
                "and S.sessao = :dataFinal"),
        @NamedQuery(name = "sessaoAtiva.byMatricula", query = "select S from Sessao  S where S.usuario.matricula  = :matricula and S.fimSessao is null"),
        @NamedQuery(name = "sessaoALL.byUser", query = "select S from Sessao  S where S.usuario.id  = :id order by S.id desc"),
        @NamedQuery(name = "sessao.CadeirasOcupadas", query = "select S.cadeira.id from Sessao  S where S.fimSessao is null")
})
public class Sessao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;


    @NotNull
    @ManyToOne
    private Usuario usuario;

    private Calendar inicioSessao;

    private Calendar fimSessao;

    @NotNull
    private Calendar sessao;

    @NotNull
    @ManyToOne
    private Cadeira cadeira;

    @Column
    private boolean primeiroEvento;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Calendar getInicioSessao() {
        return inicioSessao;
    }

    public void setInicioSessao(Calendar inicioSessao) {
        this.inicioSessao = inicioSessao;
    }

    public Calendar getFimSessao() {
        return fimSessao;
    }

    public void setFimSessao(Calendar fimSessao) {
        this.fimSessao = fimSessao;
    }

    public Calendar getSessao() {
        return sessao;
    }

    public void setSessao(Calendar sessao) {
        this.sessao = sessao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Cadeira getCadeira() {
        return cadeira;
    }

    public void setCadeira(Cadeira cadeira) {
        this.cadeira = cadeira;
    }

    public boolean isPrimeiroEvento() {
        return primeiroEvento;
    }

    public void setPrimeiroEvento(boolean primeiroEvento) {
        this.primeiroEvento = primeiroEvento;
    }


    public Sessao() {
    }
}
