package entidade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "configuracao")
public class Configuracao implements Serializable {

    @Id
    private String nome;

    @NotNull
    private String valor;

    @Version
    private Long version;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuracao that = (Configuracao) o;
        return Objects.equals(nome, that.nome) &&
                Objects.equals(valor, that.valor);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nome, valor);
    }
}
