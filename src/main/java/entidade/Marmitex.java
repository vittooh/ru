package entidade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.Calendar;

import static javax.persistence.GenerationType.*;

@Entity
@NamedQueries(value = {
        @NamedQuery(name = "marmitex_retirada", query = "select M from Marmitex  M where m.dataRetirada >= :volta and " +
                "m.dataRetirada <= :atual and m.notificou = :valor "),
        @NamedQuery(name = "marmitex_byUserSemRetirar", query = "select  M from Marmitex M where m.usuario.id = :id" +
                " and m.retirou = false and m.cancelada = false"),
        @NamedQuery(name = "marmitex_byUserDataRetirada", query = "select  M from Marmitex M where m.usuario.id = :id " +
                "and M.dataRetirada >= :dataInicial and M.dataRetirada <= :dataFinal and M.cancelada = false")
})
public class Marmitex {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Version
    private Long version;

    @NotNull
    private Integer qt;

    @NotNull
    @ManyToOne
    private Usuario usuario;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataRetirada;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataCancelamento;

    @Column
    private Boolean notificou;


    @NotNull
    private boolean cancelada;

    @Column
    private boolean retirou;

    public Marmitex() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Integer getQt() {
        return qt;
    }

    public void setQt(Integer qt) {
        this.qt = qt;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Calendar getDataRetirada() {
        return dataRetirada;
    }

    public void setDataRetirada(Calendar dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    public Boolean getNotificou() {
        return notificou;
    }

    public void setNotificou(Boolean notificou) {
        this.notificou = notificou;
    }

    public boolean isRetirou() {
        return retirou;
    }

    public void setRetirou(boolean retirou) {
        this.retirou = retirou;
    }

    public Calendar getDataCancelamento() {
        return dataCancelamento;
    }

    public void setDataCancelamento(Calendar dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }

    public boolean isCancelada() {
        return cancelada;
    }

    public void setCancelada(boolean cancelada) {
        this.cancelada = cancelada;
    }
}
