package entidade;


import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries(value = {
        @NamedQuery(name = "cadeirasAll",query = "select C.id from Cadeira C "),
        @NamedQuery(name = "cadeirasVazias", query = "select C.id from Cadeira  C where C.id not in :lista")

})
public class Cadeira {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cadeira() {
    }
}
