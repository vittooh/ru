package enviaEmail;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@LocalBean
@Stateless
public class EnviaEmail {


    @Resource(name = "java:/mail/ru")
    private javax.mail.Session session;

    public void send(String to,String subject ,String content){
        Message m = new MimeMessage(session);
        try {
            m.setText(content);
            m.setRecipient(Message.RecipientType.TO,new InternetAddress(to));
            m.setSubject(subject);
            Transport.send(m);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
