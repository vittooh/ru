package utils;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Calendar;

/**
 * Criado por vitor em 10/11/18
 **/

public class CalendarUtils {

    public static Long retornaDiferençaMinutos(Long millisStart, Long millisEnd) {
        Duration d = Duration.between(Instant.ofEpochMilli(millisStart), Instant.ofEpochMilli(millisEnd));
        return d.toMinutes();
    }
}
