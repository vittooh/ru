package schedule;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.GetResponse;
import dao.SessaoDAO;
import pojo.sessao.SessaoUsuarioEvent;

import javax.ejb.Schedule;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;

/**
 * Criado por vitor em 20/09/18
 **/

@Singleton
@Startup
public class SessaoSchedule {

    @Inject
    Connection connection;

    @Inject
    Gson gson;

    @Inject
    private SessaoDAO sessaoDAO;

    @Schedule(second = "*/25", minute = "*", hour = "*", persistent = false, info = "Tratando evento de sessão")
    public void trataEventoSessao() {
        try {
            Channel c = connection.createChannel();
            c.queueDeclare("sessao", false, false, false, null);
            for (int i = 0; i < 20; i++) {
                GetResponse r = c.basicGet("sessao", true);
                if(r == null) return;
                String z = new String(r.getBody());
                sessaoDAO.trataSessao(gson.fromJson(z, SessaoUsuarioEvent.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
