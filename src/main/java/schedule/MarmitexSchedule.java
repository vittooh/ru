package schedule;

import dao.MarmitexDao;
import entidade.Marmitex;
import enviaEmail.EnviaEmail;
import velocity.CompilaTemplate;

import javax.ejb.*;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;


@Singleton
@Startup
public class MarmitexSchedule {

    @Inject
    MarmitexDao marmitexDao;

    @Inject
    EnviaEmail enviaEmail;

    @Inject
    CompilaTemplate compilaTemplate;


    @Schedule(second = "*/20", hour = "*",minute = "*",info = "Envia notificação de marmitex pronto", persistent = false)
    public void enviaEmailMarmitexPronto() {


        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar volta15 = Calendar.getInstance();
        volta15.add(Calendar.MINUTE, -15);
        Calendar atual = Calendar.getInstance();

        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("volta", volta15);
        parametros.put("atual", atual);
        parametros.put("valor",Boolean.FALSE);

        for (Marmitex m : marmitexDao.retornaMarmitexNotificacao(parametros)) {
            HashMap<String, Object> template = new HashMap<>();
            template.put("UsuarioPojo", m.getUsuario().getNome());
            template.put("hora", sdf.format(m.getDataRetirada().getTime()));
            enviaEmail.send(m.getUsuario().getEmail(), "Marmitex Pronto para Retirada",
                   compilaTemplate.retornaTemplateMarmitex(template));
            m.setNotificou(true);
            marmitexDao.salva(m);
        }

    }
}
