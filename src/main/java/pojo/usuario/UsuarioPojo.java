package pojo.usuario;

/**
 * Criado por vitor em 01/11/18
 **/

public class UsuarioPojo {

    private boolean isIsento;

    private Long qtCreditos;

    private String nome;

    public UsuarioPojo() {
    }

    public UsuarioPojo(String nome, boolean isento, Long qtCreditos) {
        this.nome = nome;
        this.isIsento = isento;
        this.qtCreditos = qtCreditos;
    }

    public boolean isIsento() {
        return isIsento;
    }

    public void setIsento(boolean isento) {
        isIsento = isento;
    }

    public Long getQtCreditos() {
        return qtCreditos;
    }

    public void setQtCreditos(Long qtCreditos) {
        this.qtCreditos = qtCreditos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
