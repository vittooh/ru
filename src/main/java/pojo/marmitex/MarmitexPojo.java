package pojo.marmitex;


import java.util.Date;

public class MarmitexPojo {

    private Long marmitexID;

    private Long userID;

    private Date horarioRetirada;

    private Long quantidade;


    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Date getHorarioRetirada() {
        return horarioRetirada;
    }

    public void setHorarioRetirada(Date horarioRetirada) {
        this.horarioRetirada = horarioRetirada;
    }

    public MarmitexPojo() {
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }

    public Long getMarmitexID() {
        return marmitexID;
    }

    public void setMarmitexID(Long marmitexID) {
        this.marmitexID = marmitexID;
    }
}
