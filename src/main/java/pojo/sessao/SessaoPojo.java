package pojo.sessao;

public class SessaoPojo {

    private Long idSessao;

    private String sessao;

    private Long idUser;

    private Long cadeiraID;

    private String inicioSessao;

    private String fimSessao;

    private StatusSessaoPojo statusSessaoPojo;

    public String getSessao() {
        return sessao;
    }

    public void setSessao(String sessao) {
        this.sessao = sessao;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getCadeiraID() {
        return cadeiraID;
    }

    public void setCadeiraID(Long cadeiraID) {
        this.cadeiraID = cadeiraID;
    }

    public String getInicioSessao() {
        return inicioSessao;
    }

    public void setInicioSessao(String inicioSessao) {
        this.inicioSessao = inicioSessao;
    }

    public String getFimSessao() {
        return fimSessao;
    }

    public void setFimSessao(String fimSessao) {
        this.fimSessao = fimSessao;
    }

    public StatusSessaoPojo getStatusSessaoPojo() {
        return statusSessaoPojo;
    }

    public void setStatusSessaoPojo(StatusSessaoPojo statusSessaoPojo) {
        this.statusSessaoPojo = statusSessaoPojo;
    }

    public SessaoPojo() {
    }

    public Long getIdSessao() {
        return idSessao;
    }

    public void setIdSessao(Long idSessao) {
        this.idSessao = idSessao;
    }

    public SessaoPojo(String sessao, String inicioSessao, String fimSessao, StatusSessaoPojo statusSessaoPojo, Long idSessao) {
        this.sessao = sessao;
        this.inicioSessao = inicioSessao;
        this.fimSessao = fimSessao;
        this.statusSessaoPojo = statusSessaoPojo;
        this.idSessao = idSessao;
    }
}
