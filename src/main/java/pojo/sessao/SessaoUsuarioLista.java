package pojo.sessao;

import java.util.Calendar;

public class SessaoUsuarioLista {

    private Calendar horaInicio;

    private Calendar horaFim;

    private String horario;

    private Long cadeiraID;

    private String status;

    public Calendar getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Calendar horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Calendar getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(Calendar horaFim) {
        this.horaFim = horaFim;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getCadeiraID() {
        return cadeiraID;
    }

    public void setCadeiraID(Long cadeiraID) {
        this.cadeiraID = cadeiraID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SessaoUsuarioLista() {
    }

    public SessaoUsuarioLista(Calendar horaInicio, Calendar horaFim, String horario, Long cadeiraID, String status) {
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
        this.horario = horario;
        this.cadeiraID = cadeiraID;
        this.status = status;
    }
}
