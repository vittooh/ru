package pojo.sessao;

/**
 * Criado por vitor em 10/11/18
 **/

public enum  StatusSessaoPojo {
    ATRASADA,
    OK,
    NAO_INICIADA,
    EM_PROGRESSO
}
