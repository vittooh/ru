package pojo.sessao;

import java.util.Calendar;

public class SessaoUsuarioEvent {

    private String matricula;

    private Calendar data;


    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Calendar getData() {
        return data;
    }

    public void setData(Calendar data) {
        this.data = data;
    }
}
