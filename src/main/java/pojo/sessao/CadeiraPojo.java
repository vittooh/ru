package pojo.sessao;

public class CadeiraPojo {

    private Long id;

    private boolean ocupada;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isOcupada() {
        return ocupada;
    }

    public void setOcupada(boolean ocupada) {
        this.ocupada = ocupada;
    }

    public CadeiraPojo(Long id, boolean ocupada) {
        this.id = id;
        this.ocupada = ocupada;
    }
}
